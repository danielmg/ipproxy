package danielmg.ipproxy.listener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import danielmg.ipproxy.IPProxy;
@RunWith(SpringRunner.class)
@SpringBootTest
public class TCPListenerImplTest {
	private static Logger logger = LoggerFactory.getLogger(TCPListenerImplTest.class);

	@Value("${ipproxy.bytesPerSecond}")
	private int rate;

	private static final String testString="1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF"
			+ "1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF"
			+ "1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF"
			+ "1234567890ABCDEF1234567890ABCDEF1234567890ABCDEF";

	/*
	 * Tests - need to be bi-directional:
	 * Passthrough = clear -> clear (just pass packets - would not affect TLS/SSL)
	 * Secure Proxy = secure -> clear  (useful for when you need TLS but remote doesn't support)
	 * Secure Remote = clear -> remote (TLS offload)
	 * Secure both = secure -> secure (Use for TLS intercept)
	 * 
	 * bandwidth test
	 * latency test
	 */
	
	@Test
	public void testPassThrough() throws IOException, InterruptedException {
		int testSize = 100;
		
		//a far side server 
		TestServer t = new TestServer();
		new Thread(t).start();
		
		//connect to proxy
		Socket s = new Socket("localhost",5555);
		s.setTcpNoDelay(true);
		logger.info("starting test");
		long start = System.currentTimeMillis();
		for (int i=0;i<testSize;i++) {
			s.getOutputStream().write(testString.getBytes());
			synchronized(lk) {lk.wait();}
			assertEquals(testString, new String(t.buff).trim());
		}
		long elapsed = System.currentTimeMillis() - start;
		long sent =(testSize*testString.length());
		double speed = sent /(elapsed/1000);
		logger.info("test complete elapsed ms=" + elapsed + " data sent="+ sent +" bytes speed="+speed + " b/s expected: " + rate + " b/s");
		assertTrue(speed<=rate);
		s.close();
		
		t.run=false;
		if (t.srv!=null) t.srv.close();
		
		IPProxy.shutdown();
		
	}
	private Object lk = new Object();
	private class TestServer implements Runnable {
		volatile boolean run=true;
		byte[] buff = new byte[1500];
		ServerSocket srv;
		@Override
		public void run() {
			try {
				srv = new ServerSocket(6666);
				logger.info("test server open");
				Socket s=srv.accept();
				InputStream is = s.getInputStream();
				logger.info("test server got a connection from: " + s.getRemoteSocketAddress() );
				while (run) {
					is.read(buff);
					logger.debug("got " + new String(buff));
					synchronized(lk) {lk.notifyAll();}
				}
			} catch (IOException e) {
				logger.info("test server error: " + e.getMessage());
			}

		}

	}

}
