package danielmg.ipproxy.socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

public interface SocketFactory {
	
	
	public Socket createSocket(String remoteAddress, int port) throws IOException;
	public ServerSocket createServerSocket(int port) throws IOException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, CertificateException, KeyManagementException;
	
	
}
