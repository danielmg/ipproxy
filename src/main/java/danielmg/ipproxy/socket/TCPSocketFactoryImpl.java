package danielmg.ipproxy.socket;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TCPSocketFactoryImpl implements SocketFactory {
	private static Logger logger = LoggerFactory.getLogger(TCPSocketFactoryImpl.class);

	@Value("${ipproxy.socket.linger}")
	private int soLinger;

	@Value("${ipproxy.socket.backlog}")
	private int backlog;

	@Value("${ipproxy.socket.keepalive}")
	private boolean keepAlive;

	@Value("${ipproxy.socket.nowait}")
	private boolean nowait;

	@Value("${ipproxy.socket.timeout}")
	private int timeout;

	// Is the proxy Secure
	@Value("${ipproxy.server.secure.enabled}")
	private boolean secureServer;

	// path to keystore for secure proxy
	@Value("${ipproxy.server.keystore.path}")
	private String pathToServerKeyStore;

	@Value("${ipproxy.server.keystore.password}")
	private String passwordToServerKeyStore;

	// is the remote end secure
	@Value("${ipproxy.remote.secure.enabled}")
	private boolean remoteSecure;

	@Value("${ipproxy.server.secure.type}")
	private String secureServerType;
	
	@Value("${ipproxy.server.secure.suites}")
	private String serverSupportedSuites;
	
	@Value("${ipproxy.remote.secure.suites}")
	private String remoteSupportedSuites;

	@Override
	public Socket createSocket(String remoteAddress, int port) throws IOException {

		logger.info("creating client socket to:" + remoteAddress + ":" + port);
		Socket s;
		
		if (remoteSecure) {
			SSLSocket ssl = (SSLSocket)SSLSocketFactory.getDefault().createSocket(remoteAddress, port);
			ssl.setEnabledCipherSuites(getRemoteSuites());
			
			logger.info("Cipher suite: "+ ssl.getSession().getCipherSuite());
	          for (Certificate c: ssl.getSession().getPeerCertificates()) {
	        	  if (c instanceof X509Certificate) logger.info("Subject DN:" +((X509Certificate)c).getSubjectDN());
	          }
	          logger.info("ID: "+ ssl.getSession().getId());
			s=ssl;
			
		} else {
			s = new Socket(remoteAddress, port);
		}

		if (soLinger > 0) s.setSoLinger(true, soLinger);
		else s.setSoLinger(false, 0);

		s.setKeepAlive(keepAlive);
		s.setTcpNoDelay(nowait);
		s.setSoTimeout(timeout);

		return s;
	}

	@Override
	public ServerSocket createServerSocket(int port) throws IOException, NoSuchAlgorithmException, KeyStoreException,
			CertificateException, UnrecoverableKeyException, KeyManagementException {
		if (secureServer) {
			logger.info("creating secure socket on port: " + port);
			SSLContext context = SSLContext.getInstance(secureServerType);
			
			//The reference implementation only supports X.509 keys
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			
			KeyStore ks = KeyStore.getInstance("JKS");

			ks.load(new FileInputStream(pathToServerKeyStore), passwordToServerKeyStore.toCharArray());
			kmf.init(ks, passwordToServerKeyStore.toCharArray());

			context.init(kmf.getKeyManagers(), null, null);

			SSLServerSocket ss = (SSLServerSocket) context.getServerSocketFactory().createServerSocket(port);
			ss.setEnabledCipherSuites(getServerSuites());

			return ss;
		} else {
			logger.info("creating socket on port: " + port);
			return new ServerSocket(port, backlog);
		}
	}
	
	private String[] getSuites(String s) {
		if (s == null || s.trim().length()==0) {
			//default suites
			return new String[]{"TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256"};
		} else {
			String[] suites = s.split(",");
			//remove whitespace
			for (int i=0;i<suites.length;i++) suites[i]=suites[i].trim();
			return suites;
		}
	}
	private String[] getServerSuites() {
		return getSuites(serverSupportedSuites);
	}
	private String[] getRemoteSuites() {
		return getSuites(remoteSupportedSuites);
	}

}
