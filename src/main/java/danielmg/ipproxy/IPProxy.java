package danielmg.ipproxy;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import com.google.common.collect.Lists;

import danielmg.ipproxy.capture.CaptureService;
import danielmg.ipproxy.listener.Listener;
import danielmg.ipproxy.listener.TCPListenerImpl;
import danielmg.ipproxy.socket.SocketFactory;

@SpringBootApplication
@Configuration
public class IPProxy implements CommandLineRunner {
	private static Logger logger = LoggerFactory.getLogger(IPProxy.class);
	//MTU=1500 default
	public static final int BUFFER_SIZE = 1500;
	//Null byte for over-writing
	public static final byte NULL_BYTE=(byte)0;

	@Value("${ipproxy.local.startPort}")
	private int localStartPort;
	@Value("${ipproxy.local.endPort}")
	private int localEndPort;

	@Value("${ipproxy.remote.startPort}")
	private int remoteStartPort;

	@Value("${ipproxy.bytesPerSecond}")
	private int bytesPerSecond;

	@Value("${ipproxy.remote.address}")
	private String remoteAddress;

	@Value("${ipproxy.max.connections}")
	private int maxConnections;

	@Value("${ipproxy.traffic.log.enabled}")
	private boolean capture;
	
	@Value("${ipproxy.latency.artificial}")
	private long latency;
	
	@Autowired
	private SocketFactory socketFactory;
	
	@Autowired
	private CaptureService captureService;
	
	private static final List<Listener> listeners = Lists.newLinkedList();

	@Override
	public void run(String... args) throws Exception {
		logger.info("IPProxy starting up...");
		// limit the number of overall connections
		final Executor exec = Executors.newFixedThreadPool(maxConnections);

		int len = localEndPort - localStartPort;
		
		logger.info("spawning "+ len +" proxy listeners");
		
		int y = remoteStartPort;

		for (int x = localStartPort; x <= localEndPort; x++) {
			Listener l = new TCPListenerImpl(x, bytesPerSecond,latency,remoteAddress, y++, exec, socketFactory,capture,captureService);
			listeners.add(l);
			Thread t = new Thread(l);
			t.setName("Listener for port: " + x);
			t.start();
		}

		// try to shutdown gracefully to avoid freaking out the far side
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			shutdown();
		}));
		
		logger.info("IPProxy started");
	}

	public static void main(String[] args) {
		SpringApplication.run(IPProxy.class, args);
		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
			}
		}
	}
	
	public static void shutdown() {
		logger.info("IPProxy shutting down");
		for (Listener l : listeners) l.shutdown();
	}
}
