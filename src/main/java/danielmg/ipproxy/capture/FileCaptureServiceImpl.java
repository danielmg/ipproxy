package danielmg.ipproxy.capture;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

import danielmg.ipproxy.IPProxy;

@Service
/**
 * @author dmg
 * 
 * Asynchronous file logger for socket traffic
 *
 */
public class FileCaptureServiceImpl implements CaptureService {
	private static Logger logger = LoggerFactory.getLogger(FileCaptureServiceImpl.class);
	private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("ddMMyyyhhmmss");

	@Value("${ipproxy.traffic.log.path}")
	private String pathToCaptures;
	
	private Map<SocketAddress,Listener> listeners= Maps.newConcurrentMap();

	@Override
	public void capture(InetSocketAddress adr, byte[] data,int len) throws FileNotFoundException {
		
		Listener l = listeners.get(adr);
		
		//create a listener if this is the first time we have come across it
		if (l==null) { 
			l=new Listener(adr);
			listeners.put(adr,l);
			new Thread(l).start();
		}
		
		try {
			//copy and trim down - as we zero fill
			byte[] d = new byte[len];
			System.arraycopy(data, 0, d, 0, len);
			
			l.queue.put(data); 
		} catch (InterruptedException e) {
		}
		
	}

	@Override
	public void haltCapture(SocketAddress adr) {
		Listener l = this.listeners.get(adr);
		if (l!=null) l.running=false;
	}
	
	//async write to file
	private class Listener implements Runnable {
		
		private Listener(InetSocketAddress adr) throws FileNotFoundException {
			String name = LocalDate.now().format(df) +"_" + adr.getAddress() + "_" + adr.getPort();
			os = new FileOutputStream(pathToCaptures + File.pathSeparator + name+".cap");
		}
		
		volatile boolean running=true;
		OutputStream os;
		ArrayBlockingQueue<byte[]> queue = new ArrayBlockingQueue<byte[]>(Integer.MAX_VALUE);
		@Override
		public void run() {
			while (running) {
				try {
					byte[] data = queue.poll(1, TimeUnit.SECONDS);
					
					//write data if we got some
					if (data!=null&&os!=null) { 
						os.write(data);	
						//wipe out!
						Arrays.fill(data, IPProxy.NULL_BYTE);
					}
					
				} catch (InterruptedException | IOException e) {
					logger.warn("error writing capture",e);
				}
			}
			try {
				os.close();
			} catch (IOException e) {
			}
		}
		
	}
	
}
