package danielmg.ipproxy.capture;

import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

public interface CaptureService {
	
	
	public void haltCapture(SocketAddress adr);
	public void capture(InetSocketAddress adr, byte[] data, int len) throws FileNotFoundException;

}
