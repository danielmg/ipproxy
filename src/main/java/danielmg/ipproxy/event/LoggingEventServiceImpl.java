package danielmg.ipproxy.event;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;

/**
 * @author dmg
 * 
 * Asynchronous event logger for socket traffic
 *
 */
@Service
public class LoggingEventServiceImpl implements EventService {
	private static Logger logger = LoggerFactory.getLogger(LoggingEventServiceImpl.class);
	private static final DateTimeFormatter eventFormatter = DateTimeFormatter.ofPattern("dd/MM/yyy,hh:mm:ss.SSS");
	private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("ddMMyyyhhmmss");

	@Value("${ipproxy.event.log.path}")
	private String pathToEvents;

	private Map<SocketAddress,Listener> listeners= Maps.newConcurrentMap();

	@Override
	public void logEvent(InetSocketAddress adr, EventType eventType) throws FileNotFoundException {
		
		Listener l = listeners.get(adr);
		//if we don't already have this then we spawn a new listener.
		if (l==null) { 
			l=new Listener(adr);
			new Thread(l).start();
			listeners.put(adr,l);
		}
		try {
			//drop the event onto the listeners queue
			l.queue.put(eventType); 
		} catch (InterruptedException e) {
		}
		
	}

	@Override
	public void haltEventLog(SocketAddress adr) {
		//signal the listener to close down
		Listener l = this.listeners.get(adr);
		if (l!=null) l.running=false;
	}
	
	//async write to file
	private class Listener implements Runnable {
		
		private Listener(InetSocketAddress adr) throws FileNotFoundException {
			//open a file to write to
			String name = LocalDate.now().format(df) +"_" + adr.getAddress() + "_" + adr.getPort();
			os = new FileOutputStream(pathToEvents + File.pathSeparator + name+".log");
		}
		
		volatile boolean running=true;
		OutputStream os;
		ArrayBlockingQueue<EventType> queue = new ArrayBlockingQueue<EventType>(Integer.MAX_VALUE);
		
		@Override
		public void run() {
			while (running) {
				try {
					//wait for an event to come through or 1 second (to allow us to check running status.
					EventType eventType = queue.poll(1, TimeUnit.SECONDS);
					//check we have an event and somewhere to put event
					if (eventType!=null&&os!=null) {  
						//form our line
						String output=LocalDate.now().format(eventFormatter)+","+eventType.name()+"\n";
						os.write(output.getBytes());						
					}
					
				} catch (InterruptedException | IOException e) {
					logger.warn("error writing event",e);
				}
			}
			try {
				os.close();
			} catch (IOException e) {
			}
		}
		
	}
	
}
