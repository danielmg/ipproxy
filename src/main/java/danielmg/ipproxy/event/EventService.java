package danielmg.ipproxy.event;

import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

public interface EventService {
	
	public enum EventType{CLIENT_TO_PROXY,PROXY_TO_REMOTE,REMOTE_TO_PROXY,PROXY_TO_CLIENT,CLIENT_TO_PROXY_CONNECT,PROXY_TO_REMOTE_CONNECT,CLIENT_DISCCONNECT,REMOTE_DISCONNECT,REMOTE_ERROR,CLIENT_ERROR}
	public void haltEventLog(SocketAddress adr);
	public void logEvent(InetSocketAddress adr, EventType eventType) throws FileNotFoundException;

}
