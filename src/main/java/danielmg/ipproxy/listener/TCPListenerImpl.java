package danielmg.ipproxy.listener;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.GeneralSecurityException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.RateLimiter;

import danielmg.ipproxy.IPProxy;
import danielmg.ipproxy.capture.CaptureService;
import danielmg.ipproxy.socket.SocketFactory;

public class TCPListenerImpl implements Listener {
	private static Logger logger = LoggerFactory.getLogger(TCPListenerImpl.class);

	private final int port;
	
	//Default MTU for TCP
	private ServerSocket socket;
	private final Executor exec;
	private final SocketFactory socketFactory;
	private final int rate;
	private final String remoteAddress;
	private final int remotePort;
	private final boolean capture;
	private final CaptureService captureService;
	private final long latency;
	
	public TCPListenerImpl(int port, int rate, long latency, String remoteAddress, int remotePort,Executor exec,SocketFactory socketFactory, boolean capture, CaptureService captureService) {
		this.port = port;
		this.exec=exec;
		this.socketFactory=socketFactory;
		this.rate=rate;
		this.remoteAddress=remoteAddress;
		this.remotePort=remotePort;
		this.capture=capture;
		this.captureService=captureService;
		this.latency=latency;
	}

	private volatile boolean running = true;

	@Override
	public void run() {
		
		logger.info("listener thread started for port:" + port);

		try {
			socket = socketFactory.createServerSocket(port);
			logger.info("listener socket open for port:" + port);

			while (running) {
				final Socket s = socket.accept();
				
				//hand off to thread pool for reads from client
				exec.execute(() -> {
					final byte[] buff = new byte[IPProxy.BUFFER_SIZE];
					
					final RateLimiter rl = rate>0?RateLimiter.create(rate,1,TimeUnit.SECONDS):null;
					
					//dump out TLS/SSL info as needed
					if (s instanceof SSLSocket) {
						  final SSLSession session = ((SSLSocket) s).getSession();
				          for (Certificate c: session.getLocalCertificates()) {
				        	  if (c instanceof X509Certificate) logger.info("Subject DN:" +((X509Certificate)c).getSubjectDN());
				          }
				          logger.info("Cipher: " + session.getCipherSuite());
				          logger.info("Protocol: " + session.getProtocol());
				          logger.info("ID: " + session.getId());
					}
					
					try (final InputStream is = s.getInputStream(); 
						 final Sender sender = new Sender(remotePort,s.getOutputStream())) {
						logger.info("listener connection accepted on port: " + port + " from: " + s.getRemoteSocketAddress());
						// we have a connection - open the other end
						sender.open();
						
						//remote end address
						InetSocketAddress adr = (InetSocketAddress) s.getRemoteSocketAddress();
						
						//keep looping while we should and while the socket is open
						while (running&&!s.isClosed()) {
							// read packet
							int read = is.read(buff);
							if (read > 0) {
								//gatekeeper for the bandwidth limitations
								if (rate>0) rl.acquire(read);
								
								//latency wait if needed
								if (latency>0) latencyWait();
								
								//push off to our sender
								sender.send(buff,read);
								
								//dispatch to capture if needed
								if (capture) captureService.capture(adr, buff, read);
						
								//zero the buffer for security
								Arrays.fill(buff,IPProxy.NULL_BYTE);
							}
							
						}
					} catch (Exception e) {  //have to catch this for autoclosable
						if (!(e instanceof SocketException) && !s.isClosed()) { //only log if not closed etc to avoid mess around blocking reads and closing
							logger.error(e.getMessage(), e);
						}
					} finally {
						closeSilently(s);
					}
				});
			}
		} catch (IOException | GeneralSecurityException e) {
			if (!(e instanceof SocketException) && !socket.isClosed())
				//don't log this as it is socket being closed off thread - by shutdown
				logger.error(e.getMessage(), e);
		} finally {
			if (socket!=null) closeSilently(socket);
		}
		logger.info("listener thread terminating for port:" + port);
	}

	@Override
	public void shutdown() {
		logger.info("shutting down listener for port: " + port);
		this.running = false;
		if (socket != null) closeSilently(socket);
	}
	
	private void closeSilently(Socket s) {
		try {
			s.close();
		} catch (IOException e) {
		}
	}
	
	private void closeSilently(ServerSocket s) {
		try {
			s.close();
		} catch (IOException e) {
		}
	}
	
	private void latencyWait() {
		//latency wait if needed
		try {
			Thread.sleep(latency);
		} catch (InterruptedException e) {
			//ignore
		}
	}
	
	private class Sender implements AutoCloseable {
		private Socket remoteSocket;
		private final int port;
		
		//output back to client
		private final OutputStream os;
		private volatile boolean running;
	
		Sender(int port, OutputStream os) {
			this.port=port;
			this.os=os;
		}
	
		 void send(byte[] data,int len) throws IOException {
			if (remoteSocket==null || remoteSocket.isClosed()) {
				throw new SocketException("remote socket not open");
			}
			
			remoteSocket.getOutputStream().write(data,0,len);
	
		}
	
		 @Override
		 public void close() {
			try {
				logger.info("closing socket to: " + remoteAddress + " on port:" + port);
				if (remoteSocket!=null) remoteSocket.close();
				running=false;
			} catch (IOException e) {
			//why does it do this?
			}
		}
	
		public void open() throws IOException {
			this.remoteSocket=socketFactory.createSocket(remoteAddress, port);
			
			//copy back to client and log if needed
			new Thread(()-> {
				final byte[] buff = new byte[IPProxy.BUFFER_SIZE];
				final RateLimiter rl = rate>0?RateLimiter.create(rate,1,TimeUnit.SECONDS):null;
				while (running && socket!=null && !socket.isClosed()) {
					try {
						int read=remoteSocket.getInputStream().read(buff);
						if (read>0) {
							//gateway for bandwidth limiter
							if (rate>0) rl.acquire(read);
							
							//introduce any latency simulation
							if (latency>0) latencyWait();
							
							//push over to our client
							os.write(buff, 0, read);
							
							//capture the inbound packet if needed
							if (capture) captureService.capture((InetSocketAddress)remoteSocket.getRemoteSocketAddress() ,buff, read);
							
							//zero the buffer for security
							Arrays.fill(buff,IPProxy.NULL_BYTE);
						}
						
					} catch (IOException e) {
						//ignore as this thread isn't managing the socket state.
					}
				}
				
			}).start();
			
			logger.info("connection established to: " + remoteAddress + " on port:" + port);
		}
	}
}

